import { DataObject } from '@themost/data';
declare class IdentityObject extends DataObject {
    public id?: string;
    public identity: string;
    public type: string;
    public hashed?: boolean;
    public salt: string;
    static createHash(value: string, salt?: string);
}

export default IdentityObject;