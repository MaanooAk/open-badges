import { ApplicationService } from '@themost/common';
import { ExpressDataApplication } from '@themost/express';
export declare class OpenBadgeService extends ApplicationService {
    constructor(app: ExpressDataApplication);
}