import { ApplicationService } from '@themost/common';
import { DataContext, FunctionContext, ODataModelBuilder } from '@themost/data';
import { ExpressDataApplication } from '@themost/express'

/**
 * @this FunctionContext
 * @param {*=} identifier
 * @returns
 */
 async function newIRI(identifier) {
     /**
      * @type {DataContext}
      */
     const context = this.model.context;
     const origin = context.getConfiguration().getSourceAt('settings/open-badges/origin');
     if (origin == null) {
         throw new Error('Invalid application configuration. IRI origin is missing');
     }
     let entitySet = this.model.entitySet;
     if (entitySet == null) {
        const builder = context.application.getStrategy(ODataModelBuilder);
        const entitySetObject = builder.getEntityTypeEntitySet(this.model.name);
        if (entitySetObject == null) {
            throw new Error('Invalid application configuration. IRI entity type is missing');
        }
        entitySet = entitySetObject.name;
     }
     let value = identifier;
     if (value == null) {
        value = await this.newid();
     }
     return new URL(`${entitySet}/${value}`, origin).toString();
}

class OpenBadgeService extends ApplicationService {
    /**
     * 
     * @param {ExpressDataApplication} app 
     */
    constructor(app) {
        super(app);
        // extend FunctionContext
        if (typeof FunctionContext.prototype.newIRI === 'undefined') {
            FunctionContext.prototype.newIRI = newIRI;
        }
        if (app && app.container) {
            app.container.subscribe((container) => {
                if (container != null) {
                    //
                }
            });
        }
    }
}

export {
    OpenBadgeService
}