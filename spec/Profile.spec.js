import { DataConfigurationStrategy, DefaultSchemaLoaderStrategy, ODataModelBuilder, SchemaLoaderStrategy } from '@themost/data';
import {ExpressDataApplication, ExpressDataContext} from '@themost/express';
import { OpenBadgeSchemaLoader } from '../src';
import { TestUtils } from '../../../dist/server/utils';
import { OpenBadgeService } from '../src/OpenBadgeService';

describe('Profile', () => {
    /**
     * @type {ExpressDataApplication}
     */
    let app;
    /**
     * @type {ExpressDataContext}
     */
    let context;
    beforeAll(() => {
        // get universis api container
        const container = require('../../../dist/server/app');
        app = container.get(ExpressDataApplication.name);
        // register service
        app.useService(OpenBadgeService);
        // set configuration
        app.getConfiguration().setSourceAt('settings/open-badges', {
            origin: 'https://universis.example.com/ob/v2p1/'
        });
        /**
         * @type {DefaultSchemaLoaderStrategy}
         */
        const schemaLoader = app.getConfiguration().getStrategy(SchemaLoaderStrategy);
        schemaLoader.loaders.push(new OpenBadgeSchemaLoader(schemaLoader.getConfiguration()));
        // reset schema loader
        const builder = app.getStrategy(ODataModelBuilder);
        builder.clean(true);
        builder.initializeSync();
        
    });
    beforeEach(() => {
        context = app.createContext();
    });
    afterEach(async () => {
        await context.finalizeAsync();
    });

    it('should get Profile model definition', async() => {
        const Profiles = context.model('OpenBadgeProfile');
        expect(Profiles).toBeTruthy();
        await Profiles.getItems()
    });

    it('should create Profile', async() => {
        await TestUtils.executeInTransaction(context, async() => {
            const Profiles = context.model('OpenBadgeProfile');
            const newProfile = await Profiles.silent().insert({
                name: 'Test University',
                description: 'A profile that is going to be used for testing Open Badges.'
            });
            expect(newProfile).toBeTruthy();
        });
    });

});